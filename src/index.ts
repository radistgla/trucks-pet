import express, { Request, Response } from 'express';
import { Client } from 'pg';

const PORT = 3000;

const app: express.Application = express();

const client = new Client({
  user: 'postgres',
  host: 'localhost',
  database: 'trucks-pet',
  password: 'postgres',
  port: 5432
});

const start = async () => {
  try {
    await client.connect();
  } catch (e) {
    console.error(e);

    process.exit(1);
  }

  app.get('/api/trucks', async (req: Request, res: Response) => {
    try {
      const queryResult = await client.query('select * from public.trucks');

      const { rows: trucks } = queryResult;

      res.status(200).json({ trucks });
    } catch (e) {
      res.status(500);
    }
  });

  app.get('/api/trucks/:id', async (req: Request, res: Response) => {
    try {
      const { id } = req.params;

      const queryResult = await client.query('select * from public.trucks where id = $1', [id]);

      const { rows } = queryResult;

      res.status(200).json({
        truck: rows[0] || null
      });
    } catch (e) {
      res.send(500);
    }
  });

  app.listen(PORT, () => console.log(`Application started on port ${PORT}`));
};

start();
